/**
 * 
 */
package ui;

import java.util.Scanner;

import core.Connect4;
import core.Connect4ComputerPlayer;

/**
 * @author jerrytan
 *
 * Connect4TextConsole is the UI component for the Connect4 game
 * 
 */
public class Connect4TextConsole implements core.ConnectK, Connect4Interface {

	public Connect4Interface play(core.Connect4 core, char[][] data) {return this;};

	
	// private members
	private static final Scanner scan = new Scanner(System.in);
	private static int next = 0;

	// computer player
	private static boolean COMPUTER_PLAYER = false;
	private static core.Connect4ComputerPlayer ai = null;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	/**
	 * Ctor
	 * @param 
	 */
	public Connect4TextConsole() {
		
	};
	
	/**
	 * Initializes the console
	 * 
	 * @param char[][] data
	 */
	public Connect4TextConsole init(core.Connect4 core, char[][] data) throws Exception {
		//for(int i=0; i<ROW_LEN; i++) System.out.println(INIT_ROW);
		int row_len = data.length;
		int col_len = data[0].length;
		for(int i=0; i<row_len; i++) {
			for(int j=0; j<col_len; j++) {
				data[i][j] = EMPTY_CHAR;
				System.out.print(PIPE + data[i][j]);
			}//for j
			System.out.println(PIPE);
		}//for i		
		System.out.println(BEGIN_GAME);
		
		// adding exception handling
		try {
			String play_computer = scan.next();
			if(play_computer.equalsIgnoreCase("C")) { COMPUTER_PLAYER = true; core.setComputerPlayer(true); ai = new core.Connect4ComputerPlayer(); System.out.println("Start game against computer."); play(false, data); }
			else if(play_computer.equalsIgnoreCase("P")) { /* no op */ } 
			else throw new Exception(INVALID_PLAYER_CHOICE); 
		}catch(Exception x) {
			System.err.println(INVALID_PLAYER_CHOICE);
			System.out.println(DEFAULT_PLAYER_CHOICE);
		}//try catch
		
		return this;
	}//init

	/**
	 * Play method
	 * 
	 * @param boolean player_x
	 * @param char[][] data 
	 */
	public Connect4TextConsole play(boolean player_x, char[][] data) throws Exception {
		int next = -1;
		if(player_x) {
			if(COMPUTER_PLAYER) { System.out.println("Computer Player has made its move."); next = ai.play(); }
			else { System.out.println("PlayerX – your turn. Choose a column number from 1-7."); next = scan.nextInt() - 1; }
		} else {
			if(COMPUTER_PLAYER) { System.out.println("It is your turn. Choose a column number from 1-7."); next = scan.nextInt() - 1; }
			else { System.out.println("PlayerO – your turn. Choose a column number from 1-7."); next = scan.nextInt() - 1; }
		}
		//int next = -1;
		//if(COMPUTER_PLAYER) next = 1; 
		//else next = scan.nextInt() - 1;
		while(!valid_move(next)) {
			System.out.println("Invalid move.  Choose a column number from 1-7.");
			next = scan.nextInt() - 1;
		}//while

		int depth = 6;
		boolean available = false;
		while(!available) {
			if(0 == depth) {
				System.out.println("Invalid move.  Choose an available column number from 1-7.");
				next = scan.nextInt() - 1;
				depth = 6;
				continue;
			}//if
			available = data[--depth][next] == EMPTY_CHAR ? true : false;
		}//while
		
		data[depth][next] = player_x ? X_CHAR : O_CHAR;
		
		int row_len = data.length;
		int col_len = data[0].length;
		for(int i=0; i<row_len; i++) {
			for(int j=0; j<col_len; j++) {
				//data[i][j] = EMPTY_CHAR;
				System.out.print(PIPE + data[i][j]);
			}//for j
			System.out.println(PIPE);
		}//for i
		return this;
	}//play
	
	/**
	 * Checks whether move is valid
	 * 
	 * @param int next
	 */
	boolean valid_move(int next) {
		return (next >= 0 && next <= 6) ? true : false;
	}//valid_move
	
}//Connect4TextConsole