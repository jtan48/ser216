/**
 * 
 */
package ui;

//import core.Connect4;
/**
 * @author jerrytan
 *
 */
public interface Connect4Interface {
	
	public Connect4Interface init(core.Connect4 core, char[][] data) throws Exception;
	public Connect4Interface play(core.Connect4 core, char[][] data);	
	public Connect4Interface play(boolean player, char[][] data) throws Exception;	
	
}
