package ui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Ellipse;

/*
 * This is the Connect4GUI class which uses JavaFX for its GUI interface,
 * which borrows heavily from the TicTacToe program that was provided in
 * SER216 as a model program demonstrating fundamental principles of event
 * driven programming in JavaFX.
 * The Connect4GUI class implements the ui.Connect4Interface so it can be 
 * readily incorporated into the pre-existing program structure of the 
 * core.Connect4 controller program in the core package.
 * 
 *  @authors Jerry Tan and author(s) of the TicTacToe program
 *  @version 1.1 - April 5th 2019
 */
public class Connect4GUI extends Application implements ui.Connect4Interface {

	//junk dna
	public Connect4Interface init(core.Connect4 core, char[][] data) {return this;};
	public Connect4Interface play(core.Connect4 core, char[][] data) {return this;};
	public Connect4Interface play(boolean player, char[][] data) {return this;};
	
	// Indicate which player has a turn, initially it is the X player
	private char whoseTurn = 'X';
	
	//boolean indicating whether playing against computer
	private static boolean playing_computer = false;
	
	// computer player
	private core.Connect4ComputerPlayer computer_player = new core.Connect4ComputerPlayer();

	// Create and initialize cell
	private Cell[][] cell = new Cell[6][7];

	// Create and initialize a status label
	//private Label lblStatus = new Label("X's turn to play");
	private static Label lblStatus; 

	/*
	 * @param String[] args
	 */
	public static void main(String[] args) {
		if(null == args) { /* no op */ }
		else if(args[0].equalsIgnoreCase("true")) playing_computer = true;
	    //Application.launch(args);
		server.Connect4Server.main(new String[] {Boolean.toString(playing_computer)});
	}//main

	@Override // Override the start method in the Application class
	public void start(Stage primaryStage) 
	{
	
	    lblStatus = new Label("X's turn to play");
	
		// Pane to hold cell
		GridPane pane = new GridPane();
		
		for (int i = 0; i < 6; i++)
			for (int j = 0; j < 7; j++)
				pane.add(cell[i][j] = new Cell(), j, i);
	
		BorderPane borderPane = new BorderPane();
		borderPane.setCenter(pane);
		borderPane.setBottom(lblStatus);
	
		// Create a scene and place it in the stage
		Scene scene = new Scene(borderPane, 450, 170);
		primaryStage.setTitle("Jerry Tan's Connect4"); // Set the stage title
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage
	}

	/** Determine if the cell are all occupied */
	public boolean isFull() 
	{
		for (int i = 0; i < 6; i++)
			for (int j = 0; j < 7; j++)
			    if (cell[i][j].getToken() == ' ')
			    	return false;
	
		return true;
	}

	/** check win */
	/*
	 * @param Cell[][] data
	 * @return boolean
	 */
	boolean check_win(Cell[][] data) {
		int row_len = data.length;
		int col_len = data[0].length;
	    /* walk each element in data */
	    for(int row=0; row<row_len; row++) {
	      for(int col=0; col<col_len; col++) {
		        char element = data[row][col].getToken();
		        if(' ' == element) continue; // ignore empty char
		        /* check horizontal */
		        if(col <= data[row].length-4 && element == data[row][col+1].getToken() && element == data[row][col+2].getToken() && element == data[row][col+3].getToken()) return true;
		        /* check vertical */
		        if(row <= data.length-4 && element == data[row+1][col].getToken() && element == data[row+2][col].getToken() && element == data[row+3][col].getToken()) return true;
		        /* check diagonals */
		        if(row <= data.length-4 && col <= data[row].length-4) {
		          // If the current element equals each element diagonally to the bottom right
		          if(element == data[row+1][col+1].getToken() && element == data[row+2][col+2].getToken() && element == data[row+3][col+3].getToken() ) return true;
		        }//if row
		        /* check diagonals */
				if(row <= data.length-4 && col >= data[row].length-4) {
				  if(element == data[row+1][col-1].getToken() && element == data[row+2][col-2].getToken() && element == data[row+3][col-3].getToken() ) return true;
		        }//if row
	      }//for col
	    }//for row
	    return false;
	  }//check win

	/** Determine if the player with the specified token wins */
	public boolean isWon(char token) { return check_win(cell); }

	//An inner class for a cell
	public class Cell extends Pane 
	{
		// Token used for this cell
		private char token = ' ';
	
		public Cell() 
		{
			setStyle("-fx-border-color: black");
			this.setPrefSize(2000, 2000);
			this.setOnMouseClicked(e -> handleMouseClick());
		}
	
		/** Return token */
		public char getToken() {
			return token;
		}
	
		/** Set a new token */
		public void setToken(char c) 
		{
			token = c;
		
			if (token == 'X') 
			{
			    Line line1 = new Line(10, 10, this.getWidth() - 10, this.getHeight() - 10);
			    line1.endXProperty().bind(this.widthProperty().subtract(10));
			    line1.endYProperty().bind(this.heightProperty().subtract(10));
			    Line line2 = new Line(10, this.getHeight() - 10, this.getWidth() - 10, 10);
			    line2.startYProperty().bind(this.heightProperty().subtract(10));
			    line2.endXProperty().bind(this.widthProperty().subtract(10));
		
			    // Add the lines to the pane
			    this.getChildren().addAll(line1, line2);
			}
			else if (token == 'O') {
			    Ellipse ellipse = new Ellipse(this.getWidth() / 2,
			    this.getHeight() / 2, this.getWidth() / 2 - 10,
			    this.getHeight() / 2 - 10);
			    ellipse.centerXProperty().bind(this.widthProperty().divide(2));
			    ellipse.centerYProperty().bind(this.heightProperty().divide(2));
			    ellipse.radiusXProperty().bind(this.widthProperty().divide(2).subtract(10));
			    ellipse.radiusYProperty().bind(this.heightProperty().divide(2).subtract(10));
			    ellipse.setStroke(Color.BLACK);
			    ellipse.setFill(Color.WHITE);
			    getChildren().add(ellipse); // Add the ellipse to the pane
			}
		}
	
		/* Handle a mouse click event */
		private void handleMouseClick() 
		{
			 // If cell is empty and game is not over
			if (token == ' ' && whoseTurn != ' ') 
			{
				
				if('O' == whoseTurn && playing_computer) {

					//articulating our collective suspension of disbelief
					//try { Thread.sleep(2250); } catch(InterruptedException iex) { System.err.println(iex.getLocalizedMessage());}
					computer_player.play(cell);

				}//if computer's turn
				else setToken(whoseTurn); // Set token in the cell
					    
			    // Check game status
			    if (isWon(whoseTurn)) {
					lblStatus.setText(whoseTurn + " won! The game is over");
					whoseTurn = ' '; // Game is over
			    }
			    else if (isFull()) {
					lblStatus.setText("Draw! The game is over");
					whoseTurn = ' '; // Game is over
			    }
			    else {
					// Change the turn
					whoseTurn = (whoseTurn == 'X') ? 'O' : 'X';
					// Display whose turn
					if(playing_computer && 'O' == whoseTurn) lblStatus.setText(whoseTurn + "'s turn.  Click on any empty square to see Computer's move.");
					else lblStatus.setText(whoseTurn + "'s turn");
		  	    }
			}
		 }
	}
}