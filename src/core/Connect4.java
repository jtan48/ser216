/**
 * 
 */
package core;

import java.util.Scanner;

import ui.Connect4GUI;

/**
 * @author jerrytan
 *
 * Connect 4 is the core component for the Connnect4 game.
 * 
 */
public class Connect4 implements ConnectK {

	//private ui.Connect4TextConsole console;
	//private ui.Connect4GUI console;
	private ui.Connect4Interface console;
	private static char[][] data = new char[6][7];
	private static boolean player_x = true;

	// private members
	private static final Scanner scan = new Scanner(System.in);
	
	
	// computer player
	public static boolean COMPUTER_PLAYER = false;
	
	public static void setComputerPlayer(boolean flag) { COMPUTER_PLAYER = flag; }
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/***************************/
		// 
		// adding exception handling
		// NOTHING will get past this
		// because it's right at the
		// main entry point
		// Any and every exception
		// will get caught here
		// (with possible exception of
		// system errors represented inn
		// the Error class of java.lang.Throwable
		/****************************/
		ASK: try {
			Connect4 c4;
			System.out.println("Do you want to play with text console or GUI?  Choose 1 for text console, or 2 for GUI.");
			int choice = scan.nextInt();
			if(1 == choice) c4 = new Connect4(new ui.Connect4TextConsole()).init(data).play(data);
			else if (2 == choice) { 
				System.out.println("Do you want to play with a person, or against the computer?  Choose 1 for person, or 2 for computer.");
				choice = scan.nextInt();
				if(1 == choice) {
					System.out.println("Good choice.  You will need to start 2 instances of the Connect4Client: one for Player X and another for Player O. Good Luck!"); 
					Connect4GUI.main(new String[]{"false"});
				}
				else if (2 == choice) {
					System.out.println("Brave choice.  You will be Player X and the Computer will be Player O.  You will need to start the Connect4Client and the Connect4ComputerClient.  Good Luck!"); 
					Connect4GUI.main(new String[]{"true"});
					//new client.Connect4ComputerClient().main(args);
				}
				else { System.out.println("Invalid choice.  Pay attention next time."); }
			} else { System.out.println("Invalid choice.  Pay attention next time."); }
		}catch(Exception x) {
			System.err.println(x.getMessage());
			System.out.println(GRACEFUL_EXIT);
		} finally {
			// no op for now..
		}//exceptional trinity of try-catch-finally 
	}//main
	
	/**
	 * @param args 
	 */
	//ctor
	Connect4(ui.Connect4TextConsole ui) {
		console = ui;
	}//ctor
	
	//ctor
	Connect4(ui.Connect4GUI ui) {
		console = ui;
	}//ctor

	//ctor
	Connect4(ui.Connect4Interface ui) {
		console = ui;
	}//ctor

	/**
	 * @param char[][] data
	 */
	Connect4 init(char[][] data) throws Exception {
		console.init(this,data);
		//console.launch(null);
		return this;
	}//init

	/**
	 * @param char[][] data
	 */
	Connect4 play(char[][] data) throws Exception {
		//console.play(toggle_player(),data);
		while(game_on(data)) {
			console.play(toggle_player(),data);
		}//while
		return this;
	}//play
	
	/**
	 * @param 
	 * 
	 * toggle_player will toggle the player during the game
	 * 
	 */
	boolean toggle_player() {
		if(player_x) {
			player_x = !player_x;
			return true;
		} else {
			player_x = !player_x;
			return false;
		}//if else
	}//toggle_player
	
	/**
	 * @param char[][] data
	 */
	boolean game_on(char[][] data) {
		if(check_win(data)) return false;
		return !check_draw(data);
	}//game_on

	
	/**
	 * @param char[][] data
	 */
	boolean check_draw(char[][] data) {
		int row_len = data.length;
		int col_len = data[0].length;
		for(int i=0; i<row_len; i++) {
			for(int j=0; j<col_len; j++) {
				if(EMPTY_CHAR == data[i][j]) return false;
			}//for j
		}//for i
		return true;
	}//check_draw

	/**
	 * @param char[][] data
	 */
	/* |1|2|3|4|5|6|7| */
	boolean check_win(char[][] data) {
		int row_len = data.length;
		int col_len = data[0].length;
	    /* walk each element in data */
	    for(int row=0; row<row_len; row++) {
	      for(int col=0; col<col_len; col++) {
		        int element = data[row][col];
		        if(EMPTY_CHAR == element) continue; // ignore empty char
		        /* check horizontal */
		        if(col <= data[row].length-4 && element == data[row][col+1] && element == data[row][col+2] && element == data[row][col+3]) {
					announce_win(element);//System.out.println("Player " + (element == 88 ? "X" : "O") + " Won the Game.");
					return true;
		        }//if col
		        /* check vertical */
		        if(row <= data.length-4 && element == data[row+1][col] && element == data[row+2][col] && element == data[row+3][col]) {
					announce_win(element);//System.out.println("Player " + (element == 88 ? "X" : "O") + " Won the Game.");
					return true;
		        }//if row
		        /* check diagonals */
		        if(row <= data.length-4 && col <= data[row].length-4) {
		          // If the current element equals each element diagonally to the bottom right
		          if(element == data[row+1][col+1] && element == data[row+2][col+2] && element == data[row+3][col+3] ) {
						announce_win(element);//System.out.println("Player " + (element == 88 ? "X" : "O") + " Won the Game.");
						return true;
		          }//if element
		        }//if row
		        /* check diagonals */
				if(row <= data.length-4 && col >= data[row].length-4) {
				  if(element == data[row+1][col-1] && element == data[row+2][col-2] && element == data[row+3][col-3] ) {
					  announce_win(element);//System.out.println("Player " + (element == 88 ? "X" : "O") + " Won the Game.");
					  return true;
		          }//if element
		        }//if row
	      }//for col
	    }//for row
	    return false;
	  }//check win
	
	/**
	 * @param int element
	 */
	private static final void announce_win(int element) {
		if(COMPUTER_PLAYER && 88 == element) { System.out.println("Computer Player Won the Game."); }
		else if(COMPUTER_PLAYER) { System.out.println("You Won the Game."); }
		else { System.out.println("Player " + (element == 88 ? "X" : "O") + " Won the Game.");	}
	};

}//Connect4