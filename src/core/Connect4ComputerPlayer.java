/**
 * 
 */
package core;

import java.util.concurrent.*;

/**
 * @author jerrytan
 *
 * Connect4ComputerPlayer is the core component for a computer player
 * that plays the Connect4 game with a human player.
 * 
 * Currently, it's play method is a naive implementation of choosing
 * random moves, i.e., there is no ai or other strategy behind it at all.
 * That will be for the next version to implement and improve upon.
 * 
 */
public class Connect4ComputerPlayer {
	
	// max and min for Connect4 moves
	private static final int MIN_MOVE = 1;
	private static final int MAX_MOVE = 7; 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}//main
	
	/**
	 * @param none
	 * 
	 * Naive implementation of the play method, 
	 * i.e., computer generated moves.
	 * 
	 * Currently, it just picks valid moves at random,
	 * with no state information, strategy, or any
	 * semblance of ai to speak of whatsoever.
	 * 
	 */
	public final int play() {
		return ThreadLocalRandom.current().nextInt(MIN_MOVE,MAX_MOVE);
	}//play

	/**
	 * @param ui.Connect4GUI.Cell[][] cell
	 * 
	 * Naive implementation of the play method, 
	 * i.e., computer generated moves.
	 * 
	 * Currently, it just picks valid moves at random,
	 * with no state information, strategy, or any
	 * semblance of ai to speak of whatsoever.
	 * 
	 */
	public final void play(ui.Connect4GUI.Cell[][] cell) {
		int row_len = cell.length;
		int col_len = cell[0].length;
	    /* walk each element in data */  
		GETOUT:
	    for(int row=0; row<row_len; row++) {
	      for(int col=0; col<col_len; col++) {
		        char element = cell[row][col].getToken();
		        if(' ' == element) {
		        	cell[row][col].setToken('O'); // take it!
		        	break GETOUT;
		        }//if
	      }//for
	    }//for
	}//play
	
}//class
