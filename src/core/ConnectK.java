/**
 * 
 */
package core;

/**
 * Utility interface for shared constants
 * 
 * @author jerrytan
 *
 */
public interface ConnectK {
	
	static final String BEGIN_GAME = "Begin Game.  Enter ‘P’ if you want to play against another player; enter ‘C’ to play against computer.";
	static final String PIPE = "|";
	static final char EMPTY_CHAR = ' ';
	static final char X_CHAR = 'X';
	static final char O_CHAR = 'O';
	
	// exception handling
	static final String GRACEFUL_EXIT = "System encountered unexpected error.  Terminating program.  Please contact your system administrator and advise them to review system logs.  Error Code: 101 (main loop).";
	
	//computer player
	static final String INVALID_PLAYER_CHOICE = "You made an invalid selection.";
	static final String DEFAULT_PLAYER_CHOICE = "Defaulting to computer player.";

}//ConnectK